import Dexie from 'dexie';

const db = new Dexie('muslim-quality')
db.version(1).stores({
  configs: '&name',
  prayerTimes: '&month'
})

export default db;