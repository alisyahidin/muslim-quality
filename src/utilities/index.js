import axios from 'axios';
import db from '../database';

const api = {
  getPrayerTimes() {
    return new Promise((resolve, reject) => {
      db.open().then(async db => {
        const location = await db.configs.get('location')
        const method = await db.configs.get('method')
        const response = await axios.get(
          'https://api.aladhan.com/v1/calendarByCity',
          { params: { ...location, ...method } }
        )

        const data = response.data.data.map(time => {
          const { Sunset, Midnight, Imsak, ...times } = time.timings
          return {
            date: time.date.gregorian.date,
            times: times
          }
        })

        if (response.status === 200) {
          resolve(data)
        } else {
          reject('Cannot fetch API with status code: ' + response.status)
        }
      })
    })
  }
}

export default api;