import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MenuContent from './Content';
import './style.scss';

class Menu extends Component {
  state = {
    open: false
  }

  toggleMenu = open => () => {
    this.setState({
      open: open,
    });
  };

  render() {
    return (
      <div className="menu">
        <IconButton className="menu__icon" onClick={this.toggleMenu(true)}>
          <MenuIcon />
        </IconButton>
        <MenuContent open={this.state.open} toggleMenu={this.toggleMenu}/>
      </div>
    );
  }
}

export default Menu;