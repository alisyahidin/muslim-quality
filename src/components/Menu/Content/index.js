import React from 'react';
import { Link } from "@reach/router";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import HomeIcon from '@material-ui/icons/Home';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import BookIcon from '@material-ui/icons/Book';
import DateRangeIcon from '@material-ui/icons/DateRange';
import SettingsIcon from '@material-ui/icons/Settings';
import InfoIcon from '@material-ui/icons/Info';
import { createMuiTheme } from '@material-ui/core/styles';

import './style.scss';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
});

const styles = {
  menu: {
    width: 'auto',
  },
  menu__icon: {
    color: '#FFF !important'
  }
};

function MenuContent(props) {
  const { classes, open, toggleMenu } = props

  return (
    <div>
      <SwipeableDrawer
        open={open}
        onClose={toggleMenu(false)}
        onOpen={toggleMenu(true)}
      >
        <div
          tabIndex={0}
          role="button"
          onClick={toggleMenu(false)}
          onKeyDown={toggleMenu(false)}
        >
          <div className="brand">
            <h3 className="brand__text">Muslim Quality</h3>
          </div>
          <div className={classes.menu}>
            <List>
              <Link to="/">
                <ListItem button>
                  <HomeIcon className="list-item__icon"/>
                  <Typography theme={theme} className="list-item__name">
                  Home
                  </Typography>
                </ListItem>
              </Link>
              <Link to="/prayer-time">
                <ListItem button>
                  <AccessTimeIcon className="list-item__icon"/>
                  <Typography theme={theme} className="list-item__name">
                  Prayer Time
                  </Typography>
                </ListItem>
              </Link>
              <Link to="/notes">
                <ListItem button>
                  <BookIcon className="list-item__icon"/>
                  <Typography theme={theme} className="list-item__name">
                  Muslim Notes
                  </Typography>
                </ListItem>
              </Link>
              <Link to="/calendar">
                <ListItem button>
                  <DateRangeIcon className="list-item__icon"/>
                  <Typography theme={theme} className="list-item__name">
                  Muslim Calendar
                  </Typography>
                </ListItem>
              </Link>
            </List>
            <Divider />
            <List>
              <Link to="/settings">
                <ListItem button>
                  <SettingsIcon className="list-item__icon"/>
                  <Typography theme={theme} className="list-item__name">
                  Settings
                  </Typography>
                </ListItem>
              </Link>
              <Link to="/about">
                <ListItem button>
                  <InfoIcon className="list-item__icon"/>
                  <Typography theme={theme} className="list-item__name">
                  About
                  </Typography>
                </ListItem>
              </Link>
            </List>
          </div>
        </div>
      </SwipeableDrawer>
    </div>
  );
}

MenuContent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuContent);