import React from 'react';
import Calendar from 'rc-calendar';
import 'rc-calendar/dist/rc-calendar.css';
import './style.scss';

function PrayerCalendar(props) {
  return (
    <>
      <h5 className="prayer-time__title">Calendar</h5>
      <Calendar onSelect={(date) => console.log(date.format('DD[-]MM[-]YYYY'))} />
    </>
  );
}

export default PrayerCalendar;