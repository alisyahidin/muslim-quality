import React from 'react';
import './style.scss';

const Loader = props => {
  const { show } = props

  return (
    <div id="loader" className={ show ? 'show' : 'hide' }>
      <h1 id="loader__content" className={ show ? 'show' : 'hide' }>
        Loading...
      </h1>
    </div>
  )
}

export default Loader;