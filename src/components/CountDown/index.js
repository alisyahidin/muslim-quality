import React, { Component } from 'react';
import moment, { duration } from 'moment';

export default class CountDown extends Component {
  state = {
    duration: {}
  }

  static getDerivedStateFromProps(props, state) {
    return { ...state, duration: duration(props.endTime.diff(moment())) }
  }

  componentDidMount() {
    this.interval = setInterval(this.tick, 1000)
  }

  componentWillUnMount() {
    clearInterval(this.interval)
  }

  tick = () => {
    const { duration } = this.state
    const time = duration.subtract(1, 'second')

    this.setState({ duration: time })
  }

  render() {
    const { duration } = this.state

    return (
      <h1>{ `${duration.hours()}:${duration.minutes()}:${duration.seconds()}` }</h1>
    );
  }
}
