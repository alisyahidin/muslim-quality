import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    marginTop: 0,
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 100,
  },
  cellTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
    fonSize: '120%'
  },
  cell: {
    textAlign: 'center'
  }
});

function PrayerTimeList(props) {
  const { classes, data } = props;

  return (
    <>
      <h5 className="prayer-time__title">{ `${moment().format('dddd')}, ${moment().format("Do MMMM YYYY")}` }</h5>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className={classes.cellTitle}>Prayer</TableCell>
              <TableCell className={classes.cellTitle}>Time</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Object.keys(data.timings).map((time, index) => (
              <TableRow key={index}>
                <TableCell className={classes.cell}>{ time }</TableCell>
                <TableCell className={classes.cell}>{ data.timings[time] }</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </>
  );
}

PrayerTimeList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PrayerTimeList);