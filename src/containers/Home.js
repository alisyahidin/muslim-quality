import React, { Component } from 'react';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getPrayerTime } from '../actions/prayerAction';
import { CountDown } from '../components';
import './Home.scss';

class Home extends Component {
  getNextPrayer = times => {
    if (times.length === 0) return {name: '', time: ''}

    const reducedTimes = times.map(day => {
        const date = day.date
        return Object.entries(day.times).map(prayer => {
          return { [prayer[0]]: `${date} ${prayer[1]}` }
        })
      }).reduce((prev, next) => {
        return prev.concat(next)
      }).filter(prayer => {
        const prayerTime = Object.values(prayer)[0]
        return moment().isBefore(moment(prayerTime, 'DD[-]MM[-]YYYY HH:mm'))
      })

    return Object.entries(reducedTimes[0]).map(data => {
      return { name: data[0], time: data[1] }
    })[0]
  }

  componentDidMount() {
    this.props.getPrayerTime()
  }

  render() {
    const next = this.getNextPrayer(this.props.times)

    return (
      <div className="container home">
        <CountDown endTime={ moment(next.time, 'DD[-]MM[-]YYYY HH:mm') } />
        <h4>{ `${next.name} ${next.time.replace(/[0-9]{2}-[0-9]{2}-[0-9]{4}/, '')}` }</h4>
      </div>
    );
  }
}

const mapStateToProps = state => (
  {
    times: state.prayers.times
  }
)

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getPrayerTime
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);