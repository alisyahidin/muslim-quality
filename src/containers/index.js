import Home from './Home';
import PrayerTime from './PrayerTime';
import Notes from './Notes';
import Calendar from './Calendar';
import About from './About';
import Settings from './Settings';

export {
  Home,
  PrayerTime,
  Notes,
  Calendar,
  About,
  Settings }