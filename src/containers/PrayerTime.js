import React, { Component } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import {
  PrayerTimeList,
  Calendar } from '../components';
import './PrayerTime.scss';

class PrayerTime extends Component {
  render() {
    const { today, load }= this.props

    return (
      <div className="container">
        <Grid container className="prayer-time" alignItems="center" justify="center">
          <Grid item xs={12} sm={8} md={4} lg={4}>
            { !load && <PrayerTimeList data={today}/> }
          </Grid>
          <Hidden only={['xs', 'sm']}>
            <Grid item md={6} lg={6}>
              <Calendar />
            </Grid>
          </Hidden>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => (
  {
    today: state.prayerTime.today,
    load: state.prayerTime.load
  }
)

export default connect(mapStateToProps, null)(PrayerTime);