import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose } from 'redux';
import thunk from 'redux-thunk';
import prayerReducer from '../reducers/prayerReducer';

const middleware = applyMiddleware(thunk)

const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const store = createStore(
  combineReducers({
    prayers: prayerReducer
  }),
  compose(
    middleware,
    reduxDevTools
  )
)

export default store;