import React, { Component } from 'react';
import moment from 'moment';
import { Router } from '@reach/router';
import { Menu, Loader } from './components';
import {
  Home,
  PrayerTime,
  Notes,
  Calendar,
  About,
  Settings } from './containers';
import './App.scss';

import db from './database';
import api from './utilities';
import { location, method } from './config';

class App extends Component {
  state = {
    load: true
  }

  componentDidMount() {
    db.open().then(async db => {
      let count = await db.configs.count()
      if (count === 0) {
        db.configs.add({ name: 'location', ...location })
        db.configs.add({ name: 'method', ...method })
      }
    })

    db.open().then(async db => {
      const count = await db.prayerTimes.where('month').equals(moment().format('YYYY[-]MM')).count()
      if (count === 0) {
        api.getPrayerTimes()
          .then(data => {
            db.prayerTimes.add({
              month: moment().format('YYYY[-]MM'),
              data: data
            })
          })
          .then(() => this.setState({ load: false }))
      } else {
        this.setState({ load: false })
      }
    })
  }

  render() {
    const { load } = this.state

    return (
      <>
        <Menu />
        <Loader show={load} />
        { !load && <Router>
            <Home path="/" />
            <PrayerTime path="prayer-time" />
            <Notes path="notes" />
            <Calendar path="calendar" />
            <Settings path="settings" />
            <About path="about" />
          </Router> }
      </>
    );
  }
}

export default App;
