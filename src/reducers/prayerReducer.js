const defaultState = {
  times: []
}

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'GET-TIME':
      return {
        ...state, times: action.data
      }
    default:
      return state
  }
}

export default reducer;