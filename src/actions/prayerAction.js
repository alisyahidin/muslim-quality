import moment from 'moment';
import db from '../database';

export const getPrayerTime = () => {
  return dispatch => {
    db.open().then(async db => {
      const times = await db.prayerTimes.get(moment().format('YYYY[-]MM'))
      const data = times.data.filter(time => {
        const prayerTime = moment(time.date, 'DD[-]MM[-]YYYY')
        const yesterday = moment().subtract(1, 'd').format('YYYY[-]MM[-]DD')
        const afterTomorrow = moment().add(2, 'd').format('YYYY[-]MM[-]DD')

        return prayerTime.isBetween(yesterday, afterTomorrow)
      })

      dispatch({
        type: 'GET-TIME',
        data: data
      })
    })
  }
}